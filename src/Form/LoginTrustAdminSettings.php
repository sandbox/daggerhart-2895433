<?php
namespace Drupal\login_trust\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class LoginTrustAdminSettings extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'login_trust_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['login_trust.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('login_trust.settings');

    $form['email']['untrusted_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t(''),
      '#required' => TRUE,
      '#default_value' => $config->get('untrusted_email_subject')
    ];

    $form['email']['untrusted_email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Details for replacement: @ip, @user_agent, @user_agent_normalized'),
      '#required' => TRUE,
      '#default_value' => $config->get('untrusted_email_body')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->configFactory()->getEditable('login_trust.settings')
      ->set('untrusted_email_subject', $form_state->getValue('untrusted_email_subject'))
      ->set('untrusted_email_body', $form_state->getValue('untrusted_email_body'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
