<?php

namespace Drupal\login_trust\EventSubscriber;

use Drupal\login_trust\Event\UserLoginEvent;
use Drupal\login_trust\Event\UntrustedUserLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserLoginSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  public $db;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  public $request;

  /**
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * @var string
   */
  public $user_agent;

  /**
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents()
  {
    return [
      UserLoginEvent::EVENT_NAME => 'onUserLogin'
    ];
  }

  /**
   * Subscribe to the user login event dispatched
   *
   * @param \Drupal\login_trust\Event\UserLoginEvent $event
   *
   * @internal param \Drupal\user\UserInterface $account
   */
  public function onUserLogin( UserLoginEvent $event )
  {
    $this->db = \Drupal::database();
    $this->request = \Drupal::request();
    $this->account = $event->account;
    $this->user_agent = $this->getUserAgent();

    if (!$this->isFirstLogin() && $this->hasNewLoginDetails()){
      // dispatch the untrusted event
      $event = new UntrustedUserLoginEvent($this->account, [
        'ip' => $this->request->getClientIp(),
        'user_agent' => $this->request->server->get('HTTP_USER_AGENT'),
        'user_agent_normalized' => $this->user_agent
      ]);
      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event_dispatcher->dispatch(UntrustedUserLoginEvent::EVENT_NAME, $event);
    }
  }

  /**
   * Determine if the authenticated user has more than one record in the
   * login_history table
   *
   * @return bool
   */
  public function isFirstLogin()
  {
    $query = $this->db->select('login_history', 'lh')
      ->fields('lh', ['uid'])
      ->condition('lh.uid', $this->account->id());

    $result = $query->execute();
    $result->allowRowCount = TRUE;

    // this is this only record of login in the login_history
    return $result->rowCount() == 1;
  }

  /**
   * Determine if this authentication attempt is the first attempt from their
   * location and user agent
   *
   * @return bool
   */
  public function hasNewLoginDetails()
  {
    $query = $this->db->select('login_history', 'lh')
      ->fields('lh', ['uid','login'])
      ->condition('lh.uid', $this->account->id())
      ->condition('lh.hostname', $this->request->getClientIP())
      ->condition('lh.user_agent', $this->user_agent);

    $result = $query->execute();
    $result->allowRowCount = TRUE;

    // this is a login from a new device or location
    return $result->rowCount() <= 1;
  }

  /**
   * Helper method to get the user_agent using the same technique as the
   * login_history module
   */
  public function getUserAgent()
  {
    $user_agent = $this->request->server->get('HTTP_USER_AGENT');

    if (strlen($user_agent) > 255) {
      $user_agent = substr($user_agent, 0, 255);
    }

    return $user_agent;
  }
}
