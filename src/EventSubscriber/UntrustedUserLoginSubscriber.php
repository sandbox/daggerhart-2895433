<?php

namespace Drupal\login_trust\EventSubscriber;

use Drupal\login_trust\Event\UntrustedUserLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UntrustedUserLoginSubscriber implements EventSubscriberInterface {

  /**
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents()
  {
    return [
      UntrustedUserLoginEvent::EVENT_NAME => 'onUntrustedUserLogin'
    ];
  }

  /**
   * @param $event \Drupal\login_trust\Event\UntrustedUserLoginEvent
   */
  function onUntrustedUserLogin($event)
  {
    $to = $event->account->getEmail();
    $language = \Drupal::languageManager()->getDefaultLanguage();

    $params = [
      'account' => $event->account,
      'details' => $event->details,
    ];

    $mailManager = \Drupal::service('plugin.manager.mail');
    $mailManager->mail('login_trust', 'untrusted_login_notify', $to, $language->getId(), $params);
  }
}
