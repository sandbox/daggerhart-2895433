<?php

namespace Drupal\login_trust\Event;

use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 *
 * @see rules_user_login()
 */
class UntrustedUserLoginEvent extends Event {

  const EVENT_NAME = 'login_trust_untrusted_login';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * HTTP Request details
   *
   * @var array
   */
  public $details;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the user logged in.
   * @param array $details
   */
  public function __construct(UserInterface $account, array $details) {
    $this->account = $account;
    $this->details = $details;
  }

}
